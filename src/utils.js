export function sortData(sortBy, data) {
    if (!data) {
        return data
    }
    switch (sortBy) {
        case 'hot': {
            let items = data.items;
            function compare(a, b) {
                //first proximity
                if (a.proximity < b.proximity) {
                    return -1;
                }
                if (a.proximity > b.proximity) {
                    return 1;
                }
                //second hotness
                if (a.hotness < b.hotness) {
                    return 1;
                }
                if (a.hotness > b.hotness) {
                    return -1;
                }
                return 0;
            }
            items.sort(compare)
            return {
                ...data,
                items: items
            }
        }
        case 'most rated': {
            let items = data.items;
            let newItems = items.map(item => {
                let summary = data.itemReviewSummaries[item.id]
                if (typeof summary === 'undefined') {
                    summary = {
                        averageDaysToArrive: Number.MAX_SAFE_INTEGER,
                        averageRating: Number.MIN_SAFE_INTEGER,
                        numberOfReviews: 0
                    }
                } else {
                    console.log("wy in this", summary);
                    summary.averageDaysToArrive = summary?.averageDaysToArrive === null || summary?.averageDaysToArrive === undefined ? Number.MAX_SAFE_INTEGER : summary.averageDaysToArrive
                    summary.averageRating = summary?.averageRating === null || summary?.averageRating === undefined ? Number.MIN_SAFE_INTEGER : summary.averageRating
                    console.log("modified wy in this", summary);
                }
                return {
                    ...item,
                    itemReviewSummaries: summary
                }

            })
            function compare(a, b) {
                //averageRating , highest to lowest
                if (a.itemReviewSummaries?.averageRating < b.itemReviewSummaries?.averageRating) {
                    return 1;
                }
                if (a.itemReviewSummaries?.averageRating > b.itemReviewSummaries?.averageRating) {
                    return -1;
                }
                //first proximity
                if (a.proximity < b.proximity) {
                    return -1;
                }
                if (a.proximity > b.proximity) {
                    return 1;
                }
                //second hotness , highest to lowest
                if (a.hotness < b.hotness) {
                    return 1;
                }
                if (a.hotness > b.hotness) {
                    return -1;
                }
                return 0;
            }
            newItems.sort(compare)
            return {
                ...data,
                items: newItems
            }
        }
        case 'fast': {
            let items = data.items;
            //preparing data since some value are null
            let newItems = items.map(item => {
                let summary = data.itemReviewSummaries[item.id]
                if (typeof summary === 'undefined') {
                    summary = {
                        averageDaysToArrive: Number.MAX_SAFE_INTEGER,
                        averageRating: Number.MIN_SAFE_INTEGER,
                        numberOfReviews: 0
                    }
                } else {
                    summary.averageDaysToArrive = summary?.averageDaysToArrive === null || summary?.averageDaysToArrive === undefined ? Number.MAX_SAFE_INTEGER : summary.averageDaysToArrive
                    summary.averageRating = summary?.averageRating === null || summary?.averageRating === undefined ? Number.MIN_SAFE_INTEGER : summary.averageRating
                }
                return {
                    ...item,
                    itemReviewSummaries: summary
                }
            })
            function compare(a, b) {
                //averageRating , highest to lowest
                if (a.itemReviewSummaries?.averageDaysToArrive < b.itemReviewSummaries?.averageDaysToArrive) {
                    return -1;
                }
                if (a.itemReviewSummaries?.averageDaysToArrive > b.itemReviewSummaries?.averageDaysToArrive) {
                    return 1;
                }
                //first proximity
                if (a.proximity < b.proximity) {
                    return -1;
                }
                if (a.proximity > b.proximity) {
                    return 1;
                }
                //second hotness , highest to lowest
                if (a.hotness < b.hotness) {
                    return 1;
                }
                if (a.hotness > b.hotness) {
                    return -1;
                }
                return 0;
            }
            newItems.sort(compare)
            return {
                ...data,
                items: newItems
            }
        }
        case 'lessThan50': {
            let items = data.items;
            let filteredItems = items.filter((item) => item.varieties.length > 0 && item.varieties[0].basePrice?.amount <= 50)
            function compare(a, b) {
                //first proximity
                if (a.proximity < b.proximity) {
                    return -1;
                }
                if (a.proximity > b.proximity) {
                    return 1;
                }
                //second hotness
                if (a.hotness < b.hotness) {
                    return 1;
                }
                if (a.hotness > b.hotness) {
                    return -1;
                }
                return 0;
            }
            filteredItems.sort(compare)
            return {
                ...data,
                items: filteredItems
            }
        }
        default: {
            let items = data.items;
            function compare(a, b) {
                //first proximity
                if (a.proximity < b.proximity) {
                    return -1;
                }
                if (a.proximity > b.proximity) {
                    return 1;
                }
                //second hotness
                if (a.hotness < b.hotness) {
                    return 1;
                }
                if (a.hotness > b.hotness) {
                    return -1;
                }
                return 0;
            }
            items.sort(compare)
            return {
                ...data,
                items: items
            }
        }
    }
}