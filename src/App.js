import React, { useEffect } from 'react'
import Select from 'react-select'
import countries from './countries.json'
import { sortData } from './utils'
import "./App.css"
function decimals(x, decimals) {
  if (!x)
    return "";

  return x.toFixed(decimals);
}

function formatRating(x) {
  const value = decimals(x, 1);
  return value.endsWith('.0') ? decimals(x, 0) : value;
}

function Carousel(props) {
  const [id, setId] = React.useState(0);

  if (props.item.images.length <= 0)
    return <React.Fragment></React.Fragment>;
  else
    return (
      <div>
        <WallItemImage item={props.item} image={props.item.images[id]} />
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          {props.item.images.map((img, imgId) => <span key={img} className={'gogo ' + (imgId === id ? 'gone' : '')} style={{ fontSize: '25px', margin: '0 5px' }} onClick={_ => setId(imgId)}>{imgId === id ? String.fromCharCode(9675) : String.fromCharCode(9679)}</span>)}
        </div>
      </div>
    );
}

function WallItemImage(props) {
  return <a href={props.item.url}><img style={{ "width": "100%" }} className="softened" src={"https://littlebiggy.org/" + props.image} alt={props.item.name} /></a>;
}

function BuyVariety(props) {
  if (props.variety.inCart) {
    return (
      <div style={{ "marginTop": "1%" }}>
        <div className="leroy">
          <a href={props.response.checkoutUrl} className="gogo Bp3">buy</a>
        </div>
        <div className="reginald Bp3">{props.variety.qtyInCart} in cart</div>
      </div>
    );
  }

  return '';
}

function Variety(props) {
  return (
    <div style={{ "marginTop": "2%" }} className="js-variety">
      <div>
        <div className="leroy" style={{ "verticalAlign": "top" }}><span className="gogo Bp1">buy</span></div>
        <div className="reginald">
          <div className={props.item.inCart ? 'gone' : ''}>
            <span className="Bp1" dangerouslySetInnerHTML={{ __html: props.variety.description }}></span>&nbsp;&nbsp;
            <span className="Bp1">
              <span className={'price ' + props.variety.basePrice.currency}><span className="currencySymbol">{props.variety.basePrice.symbol}</span>{props.variety.basePrice.amount}</span>
              {props.variety.paymentPrices.map(paymentPrice => <span key={paymentPrice.symbol + '_' + paymentPrice.amount} className={'price ' + paymentPrice.currency}>&nbsp;&nbsp;<span className="currencySymbol">{paymentPrice.symbol}</span>{paymentPrice.amount}</span>)}
            </span>
          </div>
        </div>
      </div>
      <BuyVariety response={props.response} variety={props.variety} />
    </div>
  );
}

function WallItem(props) {
  return (
    <div className="wwItem js-item">
      <div>
        <div className="leroy">
          {props.response.userRatings[props.item.id] ? <div className="Bp3">{formatRating(props.response.userRatings[props.item.id])}/10<span className="mine">:&#125;</span></div> : ''}
        </div>
        <div className="reginald">
          <div>
            <a href={props.item.url} className={"gogo Bp0 " + (props.item.inCart ? 'gomo' : '')} dangerouslySetInnerHTML={{ __html: props.item.name }}></a>
          </div>
          <div className="Bp3" style={{ "marginBottom": "1%" }}>
            {props.response.itemReviewSummaries[props.item.id] ? <a href="{props.url}?anchor=reviews">{formatRating(props.response.itemReviewSummaries[props.item.id].averageRating)} <span className="gogo">/10</span></a> : ''}
            {props.response.itemReviewSummaries[props.item.id] ? <span style={{ "marginBottom": "2%", "marginLeft": "2%" }}>{props.response.itemReviewSummaries[props.item.id].numberOfReviews}&nbsp;<a href={props.item.url + "?anchor=reviews"} className="gogo">{props.response.itemReviewSummaries[props.item.id].numberOfReviews > 1 ? "reviews" : "review"}</a></span> : ''}
          </div>

        </div>
      </div>

      <div>
        <div className="leroy"></div>
        <div className="reginald">
          <div>
            {/* <a href={props.item.url}><img style={{"width":"100%"}} className="softened" src={"https://littlebiggy.org/" + props.item.images[0]} alt={props.item.name} /></a> */}
            {/* {props.item.images.map(img => <WallItemImage item={props.item} image={img} />)} */}
            <Carousel item={props.item} />
          </div>
          <div className="Bp3" style={{ "marginTop": "1%", "lineHeight": "120%" }} dangerouslySetInnerHTML={{ __html: props.item.description }}></div>
          <div className="Bp1" style={{ "marginTop": "3.5%", "lineHeight": "120%" }}>
            <div className="Bp1">
              from <a href={props.item.seller.url} className="gogo">{props.item.seller.name}</a>
              {props.response.sellerReviewSummaries[props.item.seller.id] ? <a href={props.item.seller.reviewsUrl} style={{ "marginLeft": "2%" }}>{formatRating(props.response.sellerReviewSummaries[props.item.seller.id].averageRating)}<span className="gogo">/10</span></a> : ''}
            </div>
            <div className="Bp1">online {props.item.seller.online}</div>
            <div className="Bp3">
              ships from {props.item.shipsFrom}
              {props.response.itemReviewSummaries[props.item.id] ? <span>&nbsp;(avg {decimals(props.response.itemReviewSummaries[props.item.id].averageDaysToArrive, 0)} {props.response.itemReviewSummaries[props.item.id].averageDaysToArrive > 1 ? "days" : "day"})</span> : ''}
            </div>
          </div>
        </div>
      </div>

      <div className="js-varieties">
        {props.item.varieties.map(x => <Variety key={x.id} response={props.response} variety={x} item={props.item} />)}
      </div>
    </div>
  );
}
function createCountrySelectOption() {
  // countries
  const countryCodes = Object.keys(countries)
  return countryCodes.map((code, index) => {
    return {
      value: code,
      label: countries[code]
    }
  })
}

function Wall(props) {
  const options = createCountrySelectOption()
  return (
    <div id="wonderwall">
      <div className="Bp2 gogo"></div>

      <div style={{ "marginBottom": "5%" }} className="Bp0">
        <div className="leroy"></div>
        <div className="reginald">
          <div>{props.response.numberOfItems} {props.response.numberOfItems > 1 ? "items" : "item"}</div>
          <div>{props.response.numberOfSellers} {props.response.numberOfSellers > 1 ? "sellers" : "seller"}</div>
        </div>
        <div className="Bp2 gogo" style={{ "margin": "10px 0px", width: "20%" }}>
          <Select placeholder={'select country...'} options={options} onChange={(v) => props.setCountryCode(v.value)} />
        </div>
        <div style={{ display: 'flex', margin: "2px 2px" }}>
          <div className={`${props.sortedBy === 'hot' ? 'gone' : ''}`} style={{ cursor: 'pointer', padding: '2px 2px' }} onClick={() => props.setSortedBy('hot')}>hot</div>
          <div className={`${props.sortedBy === 'most rated' ? 'gone' : ''}`} style={{ cursor: 'pointer', padding: '2px 2px' }} onClick={() => props.setSortedBy('most rated')}>most rated</div>
          <div className={`${props.sortedBy === 'fast' ? 'gone' : ''}`} style={{ cursor: 'pointer', padding: '2px 2px' }} onClick={() => props.setSortedBy('fast')}>fast</div>
          <div className={`${props.sortedBy === 'lessThan50' ? 'gone' : ''}`} style={{ cursor: 'pointer', padding: '2px 2px' }} onClick={() => props.setSortedBy('lessThan50')}> {`<$50`} </div>
        </div>
      </div>
      <div id="wonderwall-items">
        {props.response && props.response.items.map(x => <WallItem key={x.id} item={x} response={props.response} />)}
      </div>
    </div>
  );
}


function GetItems(countryCode) {

  const url = "https://littlebiggy.org/api/json/wall/items?" + new URLSearchParams({ shipsTo: countryCode });
  //const url = "/items.json";

  return fetch(url, {
    method: 'GET',
    //mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'APIKey': '2ECE072BBCDB48B29D1EBEB60A832984'
    }
  })
    .then(x => x.json());
}

function Loading() {
  return <div>Loading...</div>;
}

function App() {

  const [response, setResponse] = React.useState(null);
  const [error, setError] = React.useState(null);
  const [countryCode, setCountryCode] = React.useState('')
  const [sortedBy, setSortedBy] = React.useState('hot')

  React.useEffect(() => {
    console.log("get items", countryCode);
    GetItems(countryCode)
      .then(x => {
        const sortedData = sortData(sortedBy, x)
        console.log("sortedData===>", sortedData, "sortedBy", sortedBy);
        setResponse(sortedData);
        //scroll to top
        window.scrollTo(0, 0);
      })
      .catch(x => {
        console.error(x);
        setError(x);
      });
  }, [countryCode])

  React.useEffect(() => {
    const sortedData = sortData(sortedBy, response)
    console.log("sortedData===>", sortedData, "sortedBy", sortedBy);
    setResponse(sortedData);
    //scroll to top
    window.scrollTo(0, 0);
  }, [sortedBy, countryCode])

  return (
    <React.StrictMode>
      <div>
        {error === null ? '' : <div className="errorContainer Bp3" style={{ marginBottom: '3%' }}></div>}

        {response === null ? <Loading /> : <Wall response={response} setCountryCode={setCountryCode} setSortedBy={setSortedBy} sortedBy={sortedBy} />}

      </div>
    </React.StrictMode>
  );
}

export default App;